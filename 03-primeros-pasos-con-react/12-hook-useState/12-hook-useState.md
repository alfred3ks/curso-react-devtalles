## Hook useState:

Ahora veremos los hook de React.js, esto es una de las cosas fantasticas que ha introducido React.js.

A partir de la version 16.8 se introdujeron los hooks.

Veremos como en nuestro componente CounterApp.jsx podemos usar el hook useState() para manejar estados de nuestro componente.

El hook useState es una de las herramientas más fundamentales en React para manejar el estado en una aplicación. Es una forma sencilla de crear y actualizar variables de estado en un componente de React sin tener que escribir una clase completa.

Un hook no es mas que una funcion.

Para profundizar tenemos la documentación:

Hooks: https://react.dev/reference/react

Veamos como queda en el componente CounterApp.jsx:

```jsx
import PropTypes from 'prop-types';
// Importamos el hook para poderlo usar:
import { useState } from 'react';

// creamos el componente CounterApp:
const CounterApp = ({ value }) => {
  // Vemos el primer hooks: Siempre siempre se debe usar en la cabecera del componente:
  const [counter, setCounter] = useState(value);

  // Creamos las funciones que pasaremos al evento click:
  const handleAdd = () => {
    setCounter(counter + 1);
    // otra forma de hacerlo
    // setCounter((counter) => {
    //   return counter + 1;
    // });
  };

  const handleSub = () => {
    setCounter(counter - 1);
  };

  const handleReset = () => {
    setCounter(0);
  };
  return (
    <>
      <h1>Contador: {counter}</h1>
      <button onClick={handleAdd}>+1</button>
      <button onClick={handleSub}>-1</button>
      <button onClick={handleReset}>Reset</button>
    </>
  );
};

// Los tipos de las props que recibe el componente:
CounterApp.propTypes = {
  value: PropTypes.number.isRequired,
};
export default CounterApp;
```
