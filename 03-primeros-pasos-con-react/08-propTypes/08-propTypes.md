## PropTypes:

Vamos a ver una caracteristicas que nos ofrece React, los PropType.
Los tipos de las propiedades de los componentes:

propTypes en React es un paquete que podemos descargar en nuestro código. Este paquete viene a ser, básicamente, un tipo de chequeo en tiempo de ejecución de las props que estamos pasando a los distintos componentes.

Es decir, si definimos que para un componente las props son de determinada manera y le pasamos una prop con otro tipo, estaremos usando el componente de manera errónea. En estos casos, React nos emitirá advertencias desde la consola, conocidas como warnings. Para esto, React revisará las props que estamos pasando a un componente y nos dirá qué tipo de prop estamos pasando frente al tipo que deberíamos pasar.

Con propTypes en React, el código no va a fallar por las advertencias. El código fallará solamente porque le tenemos un componente erróneo que falla.

OJO si hacemos un proyecto usando con CRA estas ya vienen instaladas pero si usamos Vite no vienen.

Seguimos trabajando con los archivos de App.jsx y FirstApp.jsx.

Vamos a instalarla:

```ssh
$ npm install prop-types
```

Las PropTypes nos sirve para definir los tipos a las props. Si es un string, si es un valor requerido, etc.

Podemos ver como quedan definidas las propiedades en el componente App.jsx y FirstApp.jsx:

```jsx
import HelloWorld from './components/HelloWord';
import FirstApp from './components/FirstApp';

const App = () => {
  return (
    <>
      <HelloWorld />
      <FirstApp title={'Hola soy Goku'} subTitle={22} />
    </>
  );
};

export default App;
```

```jsx
import PropTypes from 'prop-types';

// My first component in React.js:
const FirstApp = ({ title, subTitle }) => {
  return (
    <>
      <h1>First App</h1>
      <p>{title}</p>
      <p>{subTitle}</p>
    </>
  );
};

/*
Asi definimos las propiedades del componente
Podemos ver que las propiedades que va a recibir nuestro componente, podemos decir el typo y si es requerido, es una mejor manera de tener control.
*/
FirstApp.propTypes = {
  title: PropTypes.string.isRequired,
  subTitle: PropTypes.number,
};

export default FirstApp;
```

Seguimos...
