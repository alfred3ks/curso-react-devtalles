import PropTypes from 'prop-types';

// My first component in React.js:
const FirstApp = ({ title, subTitle }) => {
  return (
    <>
      <h1>First App</h1>
      <p>{title}</p>
      <p>{subTitle}</p>
    </>
  );
};

/*
Asi definimos las propiedades del componente
Podemos ver que las propiedades que va a recibir nuestro componente, podemos decir el typo y si es requerido, es una mejor manera de tener control.
*/
FirstApp.propTypes = {
  title: PropTypes.string.isRequired,
  subTitle: PropTypes.number,
};

export default FirstApp;
