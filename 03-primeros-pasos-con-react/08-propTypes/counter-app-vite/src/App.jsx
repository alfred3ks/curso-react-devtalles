import HelloWorld from './components/HelloWord';
import FirstApp from './components/FirstApp';

const App = () => {
  return (
    <>
      <HelloWorld />
      <FirstApp title={'Hola soy Goku'} subTitle={22} />
    </>
  );
};

export default App;
