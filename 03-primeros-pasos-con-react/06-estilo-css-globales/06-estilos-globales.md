## Como poner estilos globales a la aplicación:

Vamos a ver como poner estilos a un componente. React en esto es bastante abierta. Podemos usar un framework, estilos nativos, etc.

Vamos a ver como colocar estilos globales a nuestra app. Esto se aplicara en todos los componentes.

Vamos a src y creamos una carpeta que llamaremos css: Dentro de esta carpeta vamos a colocar los estilos globales de la app, crearemos un archivo llamado style.css.

```css
html,
body {
  background-color: #21232a;
  color: white;
  font-family: Arial, Helvetica, sans-serif;
  font-size: 1.3rem;
  padding: 70px;
}
```

Ahora como esto es un estilo global lo debemos llamar en main.jsx:

```jsx
import React from 'react';
import ReactDOM from 'react-dom/client';
import App from './App.jsx';
import './css/style.css';

ReactDOM.createRoot(document.getElementById('root')).render(
  <React.StrictMode>
    <App />
  </React.StrictMode>
);
```

Como podemos ver basta con importar ese archivo .css usando la instrucción import en el componente main.jsx y ya se aplicara todos los estilos generales a la app.
