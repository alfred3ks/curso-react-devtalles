// My first component in React.js:
// Si esa variable no va a cambiar cuando se actualiza el componente lo ideal es declararla fuera del renderizado del componente como es el caso:
// Asi react no reprocesa si esta fuera del componente:
const nombre = 'Alfredo';
const newMessage = 'On fire';
const bool = true;
const arr = [1, 2, 3, 4, 5, 6, 7, 8, 9];
/*
Los objetos no son permitidos como un react child, NO PODEMOS MANDA TODO EL OBJETO, podriamos mandarlo si usamodo obj.message o obj.title.
Pero podriamos enviar el objeto si lo pasamos por el metodo JSON.stringify() Asi lo que hacemos es serializar el objeto y lo muestra en joson

*/
const usuario = { name: 'Marcos', age: 23, isDev: true };

// Creamos una funcion para saludar:
const saludar = (nombre) => {
  return `Hola ${nombre}, ¿Como estas?`;
};

// Vamos a mostrar una lista de amigos:
const amigos = ['Juan', 'Juliet', 'Layka'];

const FirstApp = () => {
  // Dentro de las {} podemos pasar una expresión de JS que no sea un objeto:
  return (
    <>
      <h1>{newMessage}</h1>
      <p>{nombre}</p>
      {/* Los valores booleanos no se imprimen en html */}
      <p>{bool}</p>
      <p>{arr}</p>
      <p>{usuario.title}</p>
      <p>{usuario.message}</p>
      <code>{JSON.stringify(usuario)}</code>
      <p>{saludar(nombre)}</p>

      <p>Lista de amigos:</p>
      <ul>
        {amigos.map((amigo, index) => {
          return <li key={index}>{amigo}</li>;
        })}
      </ul>
    </>
  );
};

export default FirstApp;
