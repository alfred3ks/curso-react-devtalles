// My first component in React.js:

// props es un objeto {} si pasamos un objeto llamado info={}, seria props={info={}}
// eslint-disable-next-line react/prop-types
const FirstApp = (props) => {
  // eslint-disable-next-line react/prop-types
  const { title, msg } = props.info;
  // eslint-disable-next-line react/prop-types
  const { nombre } = props;
  return (
    <>
      <h1>First App</h1>
      <h2>{title}</h2>
      <p>{msg}</p>
      <p>{nombre}</p>
    </>
  );
};

export default FirstApp;
