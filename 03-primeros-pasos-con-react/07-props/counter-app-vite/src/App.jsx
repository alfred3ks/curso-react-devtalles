import HelloWorld from './components/HelloWord';
import FirstApp from './components/FirstApp';

const App = () => {
  const info = {
    title: 'Soy un titulo',
    msg: 'Este es parte del mensaje',
  };

  const nombre = 'Luis';

  return (
    <>
      <HelloWorld />
      <FirstApp info={info} nombre={nombre} />
    </>
  );
};

export default App;
