## Comunicación entre componentes - props:

Veremos las props, las propiedades que puede recibir un componente.
Seguimos trabajando con nuestro componente FirstApp.jsx:

Aqui vemos como desde un componente padre podemos pasar propiedades al hijo.
La información fluye desde el componente padre al hijo, nunca al reves.

Vemos como desde el componente padre pasamos props:
App.jsx:

```jsx
import HelloWorld from './components/HelloWord';
import FirstApp from './components/FirstApp';

const App = () => {
  const info = {
    title: 'Soy un titulo',
    msg: 'Este es parte del mensaje',
  };

  const nombre = 'Luis';

  return (
    <>
      <HelloWorld />
      <FirstApp info={info} nombre={nombre} />
    </>
  );
};

export default App;
```

Y las recibe el hijo: FirstApp.jsx:

```jsx
// My first component in React.js:

// props es un objeto {} si pasamos un objeto llamado info={}, seria props={info={}}
// eslint-disable-next-line react/prop-types
const FirstApp = (props) => {
  // eslint-disable-next-line react/prop-types
  const { title, msg } = props.info;
  // eslint-disable-next-line react/prop-types
  const { nombre } = props;
  return (
    <>
      <h1>First App</h1>
      <h2>{title}</h2>
      <p>{msg}</p>
      <p>{nombre}</p>
    </>
  );
};

export default FirstApp;
```
