## ¿Que es un componente de React.js?

Un componente es una pequeña pieza de código encapsulada re-utilizable que puede tener estado o no.
Solo para tener una idea de lo que es un componente podemos ver la imagen el time laps de twitter:

![twitter](../img/componente.png)

Ahora si vemos pensando mas en una app de react la estructura que podemos pensar que pued tener seria la siguiente:

TwitterApp > Router > Screen/Pagina > Menu > MenuItem, Nos quedaria asi como vemos en la imagen a continuación:
Cada cuadro podemos considerarlo como un componente:

![](../img/componente1.png)

Ahora cuando hemos hablado de estado, que es eso del estado.
Cuando un componente es renderizado por primera vez tiene un estado inicial, ese estado inicial es como se encuentra la información de ese componente la primera vez.
Podemos ver para un ejemplo de un formulario:

![](../img/formulario.png)

Luego vienen los usuario y agregan información, cada vez que el usuario cambia cualquier cosa, rellena cada item eso cambia el estado.

![](../img/formulario1.png)
