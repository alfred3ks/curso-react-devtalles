import HelloWorld from './components/HelloWord';
import FirstApp from './components/FirstApp';

const App = () => {
  return (
    <>
      <HelloWorld />
      <FirstApp />
    </>
  );
};

export default App;
