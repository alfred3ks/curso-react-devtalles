## Creación de mi primer componente - Tarea:

La idea en esta clase es crear un componente, mi primer componente y debemos llamarlo FirstApp.jsx.

Para crear este componente lo haremos en la carpeta src/components:

```jsx
// My first component in React.js:
const FirstApp = () => {
  return <h1>First Component</h1>;
};

export default FirstApp;
```

```jsx
import HelloWorld from './components/HelloWord';
import FirstApp from './components/FirstApp';

const App = () => {
  return (
    <>
      <HelloWorld />
      <FirstApp />
    </>
  );
};

export default App;
```
