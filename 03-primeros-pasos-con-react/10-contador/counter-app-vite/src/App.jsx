import CounterApp from './components/CounterApp';

const App = () => {
  return (
    <>
      <CounterApp value={22} />
    </>
  );
};

export default App;
