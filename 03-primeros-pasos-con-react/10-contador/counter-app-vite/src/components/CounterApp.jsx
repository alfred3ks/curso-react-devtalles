import PropTypes from 'prop-types';

// creamos el componente CounterApp:
const CounterApp = ({ value }) => {
  return (
    <>
      <h1>CounterApp</h1>
      <h2>{value}</h2>
    </>
  );
};

// Los tipos de las props que recibe el componente:
CounterApp.propTypes = {
  value: PropTypes.number.isRequired,
};

export default CounterApp;
