## Creamos un contador:

Para crear este contador vamos a crearlo dentro de la carpeta src/component, lo voy a llamar CounterApp.jsx.

1. Crear un nuevo componente dentro de la carpeta src llamado CounterApp.jsx.
2. El CounterApp debe ser un Funtional Component.
3. El contenido del CounterApp debe ser:

```html
<h1>CounterApp</h1>
<h2>{value}</h2>
```

4. Donde "value" es una propiedad enviada desde el padre hacia el componente CounterApp(Debe ser numerica validada con PropTypes).
5. Importar el componente CounterApp.jsx en el componente App.js.
6. Asegurarse por consila no tener ningun warning.

Nuestro componente queda asi:

```jsx
import PropTypes from 'prop-types';

// creamos el componente CounterApp:
const CounterApp = ({ value }) => {
  return (
    <>
      <h1>CounterApp</h1>
      <h2>{value}</h2>
    </>
  );
};

// Los tipos de las props que recibe el componente:
CounterApp.propTypes = {
  value: PropTypes.number.isRequired,
};

export default CounterApp;
```

```jsx
import CounterApp from './components/CounterApp';

const App = () => {
  return (
    <>
      <CounterApp value={22} />
    </>
  );
};

export default App;
```
