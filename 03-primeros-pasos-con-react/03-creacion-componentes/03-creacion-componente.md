## Vamos a crear nuestra primer componente de React.js:

Dentro de la carpeta src vamos a crear una carpeta llamada componentes:
Ahi crearemos nuestro primer componente llamado HelloWorld.jsx:

<p>📁 src</p>
<ul>
  <li>📂 components
    <ul>
      <li>📄 HelloWorld.jsx</li>
    </ul>
  </li>
</ul>

```jsx
const HelloWorld = () => {
  return <h1>Hello World!!!</h1>;
};

export default HelloWorld;
```

Luego es componente lo importamos en el componente de nuestra aplicacion, para este caso de ejemplo se llama App.jsx pero si es un proyecto tranquilamente si es un porfolio la podemos llamar Porfolio,jsx:

```jsx
// Nuestro primer componente:
const HelloWorld = () => {
  return <h1>Hello World!!!</h1>;
};

export default HelloWorld;
```
