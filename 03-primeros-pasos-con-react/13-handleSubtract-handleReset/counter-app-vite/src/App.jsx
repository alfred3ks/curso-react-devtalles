import CounterApp from './components/CounterApp';

const App = () => {
  return (
    <>
      <CounterApp value={0} />
    </>
  );
};

export default App;
