import PropTypes from 'prop-types';
import { useState } from 'react';

// creamos el componente CounterApp:
const CounterApp = ({ value }) => {
  const [counter, setCounter] = useState(value);

  // Creamos las funciones que pasaremos al evento click:
  const handleAdd = () => {
    setCounter(counter + 1);
  };

  const handleSub = () => {
    setCounter(counter - 1);
  };

  const handleReset = () => {
    setCounter(0);
  };
  return (
    <>
      <h1>Contador: {counter}</h1>
      <button onClick={handleAdd}>+1</button>
      <button onClick={handleSub}>-1</button>
      <button onClick={handleReset}>Reset</button>
    </>
  );
};

// Los tipos de las props que recibe el componente:
CounterApp.propTypes = {
  value: PropTypes.number.isRequired,
};
export default CounterApp;
