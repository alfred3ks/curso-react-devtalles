## Defaults Prop:

Cuando creamos un componente ya vimos como generar los tipos de las props que va a recibir ese comopnente, ahora veremos como definir props por default, una opcion es pasarla por parametro como lo vemos a continuacion en el componente FirstApp.jsx:

```jsx
import PropTypes from 'prop-types';

// My first component in React.js:
const FirstApp = ({
  title = 'Soy el titulo',
  subTitle = 'Yo soy un subtitulo',
}) => {
  return (
    <>
      <h1>First App</h1>
      <p>{title}</p>
      <p>{subTitle}</p>
    </>
  );
};

/*
Asi definimos las propiedades del componente
Podemos ver que las propiedades que va a recibir nuestro componente, podemos decir el typo y si es requerido, es una mejor manera de tener control.
*/
FirstApp.propTypes = {
  title: PropTypes.string.isRequired,
  subTitle: PropTypes.number,
};

export default FirstApp;
```

Esto esta bien si queremos pasar pocas props con un valor por defecto pero si nuestro componente recibe muchas props lo ideal es definir esas props en un objeto igual como hicimos con prpTypes.

Lo vemos como queda en el componente:

```jsx
import HelloWorld from './components/HelloWord';
import FirstApp from './components/FirstApp';

const App = () => {
  return (
    <>
      <HelloWorld />
      <FirstApp subTitle={22} />
    </>
  );
};

export default App;
```

OJO el orden en que entran las propType y las propDefault es primero las type y luego las default.

```jsx
import PropTypes from 'prop-types';

// My first component in React.js:
const FirstApp = ({ title, subTitle }) => {
  return (
    <>
      <h1>First App</h1>
      <p>{title}</p>
      <p>{subTitle}</p>
    </>
  );
};

/*
Asi definimos las propiedades del componente
Podemos ver que las propiedades que va a recibir nuestro componente, podemos decir el typo y si es requerido, es una mejor manera de tener control.
*/
FirstApp.propTypes = {
  title: PropTypes.string.isRequired,
  subTitle: PropTypes.number,
};

/*
Props por defecto:
Este tipo de props por defecto se activara si no enviamos nada.
Podemos definir todas las props por defecto que necesitemos.
*/
FirstApp.defaultProps = {
  title: 'Aqui viene un titulo por props por default',
  subTitle: 10,
};

export default FirstApp;
```

Este tipo de control que aplicamos a los props deben ir al final del componente.
