## Creación de una App de React.js.

A continuación veremos como crear una app de react desde cero.
Para crear nuestro proyecto usaremos a vite, pero que es vite:

Vite es un entorno de desarrollo y construcción de proyectos web moderno y rápido para JavaScript y TypeScript. Fue creado por Evan You, el mismo desarrollador detrás del popular marco de JavaScript Vue.js. El objetivo principal de Vite es proporcionar un flujo de desarrollo rápido y eficiente para aplicaciones web al minimizar el tiempo de compilación y recarga en el navegador durante el desarrollo.

Las características clave de Vite incluyen:

1. **Desarrollo rápido**: Vite utiliza una arquitectura de servidor de desarrollo altamente optimizada que proporciona tiempos de recarga en el navegador casi instantáneos, lo que lo hace ideal para un desarrollo ágil y rápido.

2. **Módulos nativos de ECMAScript (ESM)**: Vite utiliza módulos nativos de ES6 para cargar dependencias, lo que evita la necesidad de agrupar y compilar código durante el desarrollo. Esto mejora la velocidad de desarrollo y el rendimiento de la aplicación.

3. **Soporte para múltiples lenguajes**: Vite admite proyectos escritos en JavaScript, TypeScript y otros lenguajes que se compilan a JavaScript, como CoffeeScript.

4. **Recarga en caliente (Hot Module Replacement, HMR)**: Vite admite HMR, lo que significa que los cambios en el código se reflejan instantáneamente en la aplicación en ejecución sin necesidad de recargar la página completa.

5. **Personalización y extensibilidad**: Vite es configurable y extensible a través de plugins y configuraciones personalizadas. Puedes ajustar la configuración según las necesidades de tu proyecto.

6. **Optimización de producción**: Aunque Vite se enfoca en la velocidad de desarrollo, también proporciona opciones para la construcción y optimización de tu aplicación para producción, incluyendo la generación de archivos minificados y optimizados para el rendimiento.

Vite se utiliza comúnmente para desarrollar aplicaciones web de una sola página (SPA), aplicaciones Vue.js, React, Svelte y otros tipos de proyectos web modernos. Su enfoque en la velocidad de desarrollo y la eficiencia lo hace atractivo para desarrolladores que buscan un flujo de trabajo más rápido y fluido.

Para ver como crear un proycto tenemos que ir a su documentación y ver como hacerlo:

Visita la pagina de Vite: https://vitejs.dev/

Para instalar usaremos los siguientes comandos:

## Instalación de Node.js y npm

Para comenzar, asegúrate de tener Node.js y npm (el administrador de paquetes de Node.js) instalados en tu sistema. Puedes descargar e instalar Node.js desde el sitio web oficial:

Node.js: https://nodejs.org/

## Crear un proyecto de React con Vite

1. Abre tu terminal y ejecuta el siguiente comando para crear un nuevo proyecto de React con Vite:

```bash
npx create-vite@latest my-react-app --template react
```

Este comando creará un nuevo directorio llamado "my-react-app" con una configuración preconfigurada para React.

2. Cambia al directorio del proyecto recién creado:

```bash
cd my-react-app
```

3. Instala las dependencias del proyecto ejecutando el siguiente comando:

```bash
npm install
```

## Iniciar el servidor de desarrollo

Una vez que hayas creado el proyecto y hayas instalado las dependencias, puedes iniciar el servidor de desarrollo de Vite con el siguiente comando:

```bash
npm run dev
```

Esto iniciará el servidor de desarrollo de Vite y abrirá automáticamente tu proyecto en el navegador. Ahora puedes comenzar a desarrollar tu aplicación de React utilizando Vite.

## Construir la aplicación para producción

Cuando estés listo para construir tu aplicación de React para producción, puedes usar el siguiente comando:

```bash
npm run build
```

Esto generará una versión optimizada de tu aplicación en la carpeta "dist". Puedes distribuir esta carpeta en tu servidor web para poner en producción tu aplicación de React.

¡Listo! Ahora tienes un proyecto de React configurado y funcionando con Vite. Puedes comenzar a trabajar en tu aplicación React de manera eficiente utilizando Vite como entorno de desarrollo.

Procedemos a instalar nuestra aplicacion:

```bash
npx create-vite@latest counter-app-vite --template react
```

Ya tenemos nuestra aplicacion lista: Seguimos paso que nos dice la consola:

```bash
cd 03-counter-app-vite
```

```bash
npm install
```

Ya tenemos nuestra app de react para empezar a trabajar.
