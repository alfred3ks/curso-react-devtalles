## Creamos un contador y vamos a ver el evento click:

Vamos a ver como hacer click en un boton y luego pase algo. Para ver los eventos en react podemos ver su documentacion:

[Eventos en React.js](https://legacy.reactjs.org/docs/events.html)
https://legacy.reactjs.org/docs/events.html

Vamos a colocar un evento a el boton, cuando hagamos click.
Hay una cosa curiosa que debemos saber al enviar sobre el evento onClick={}, podemos pasar directamente la funcion handle o podemos pasarle una funcion y en el cuerpo pasarle la funcion que se ejecutara cuando se haga click.
Es practicamente lo mismo:

```html
<button
  onClick={(e) => {
    handleAdd(e, value);
      }}
    >
    +1
</button>
```

Como vemos en el ejemplo el evento onClick recibe por parametros un función y en su cuerpo la funcion que ejecutara en evento. Recibe por parametro el evento. De esta manera puede darse el caso que la funcion deba recibir mas parametros como el ejemplo:

```html
<button onClick="{handleAdd}">+1</button>
```

Para este caso es lo mismo que arriba solo que aqui lleva de manera implicita el evento.
La funcion que ejecuta ese evento es:

```js
// Funcion que le pasamos al evento onclick:
const handleAdd = (e, value) => {
  console.log(e);
  console.log(value);
};
```

Vemos como queda todo el componente:

```jsx
import PropTypes from 'prop-types';

// creamos el componente CounterApp:

const CounterApp = ({ value }) => {
  // Si la funcion no ocupa nada del componente la podemos colocar fuera del componente:
  // Funcion que le pasamos al evento onclick:
  const handleAdd = (e) => {
    console.log(e);
  };
  return (
    <>
      <h1>Contador: {value}</h1>
      <button onClick={handleAdd}>+1</button>
    </>
  );
};

// Los tipos de las props que recibe el componente:
CounterApp.propTypes = {
  value: PropTypes.number.isRequired,
};

export default CounterApp;
```
