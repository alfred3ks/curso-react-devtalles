import PropTypes from 'prop-types';

// My first component in React.js:
const FirstApp = ({ title, subTitle }) => {
  return (
    <>
      <h1>First App</h1>
      <p>{title}</p>
      <p>{subTitle}</p>
    </>
  );
};

/*
Asi definimos las propiedades del componente
Podemos ver que las propiedades que va a recibir nuestro componente, podemos decir el typo y si es requerido, es una mejor manera de tener control.
*/
FirstApp.propTypes = {
  title: PropTypes.string.isRequired,
  subTitle: PropTypes.number,
};

/*
Props por defecto:
Este tipo de props por defecto se activara si no enviamos nada:
Podemos definir todas las props por defecto que necesitemos.
*/
FirstApp.defaultProps = {
  title: 'Aqui viene un titulo por props por default',
  subTitle: 10,
};

export default FirstApp;
