import PropTypes from 'prop-types';

// creamos el componente CounterApp:

const CounterApp = ({ value }) => {
  // Si la funcion no ocupa nada del componente la podemos colocar fuera del componente:
  // Funcion que le pasamos al evento onclick:
  const handleAdd = (e) => {
    console.log(e);
  };
  return (
    <>
      <h1>Contador: {value}</h1>
      <button onClick={handleAdd}>+1</button>
    </>
  );
};

// Los tipos de las props que recibe el componente:
CounterApp.propTypes = {
  value: PropTypes.number.isRequired,
};

export default CounterApp;
