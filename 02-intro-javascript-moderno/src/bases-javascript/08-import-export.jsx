/*
Import, Export y métodos comunes de arreglos.
https://developer.mozilla.org/es/docs/Web/JavaScript/Reference/Global_Objects/Array/find
*/

// vamos a ver como importar y exportar archivos:
/*
Tenemos varias formas de exportar un archivo:

- 📌 Export por default:
export default heroes;
Asi lo importamos:
import heroes from './data/heroes'
heroes.map((heroe) => {
  console.log(heroe);
});

- 📌 Si tenemos mas elementos podemos exportarlos asi:
export {
  heroes,
  suma
}
Asi lo importamos:
import {heroes, suma} from './data/heroes'
heroes.map((heroe) => {
  console.log(heroe);
});

const sum = suma(12, 7);
console.log(sum);

- 📌 Podemos exportar solo un elemento:
export const heroes = [{}]
export let suma = (a, b) => {
  return a + b;
};
Asi lo importamos:
import {heroes, suma} from './data/heroes'
heroes.map((heroe) => {
  console.log(heroe);
});

const sum = suma(12, 7);
console.log(sum);

📌 - Si tenemos mas elementos podemos exportarlos asi y asignar uno por defecto:
export {
  heroes as default,
  suma
}
Asi lo importariamos:
import heroes, {suma} from './data/heroes'

*/

import heroes from '../data/heroes';
console.log(heroes);

// Vamos a ver como usar los métodos para buscar en un arreglo:
// El método find() devuelve el valor del primer elemento del array que cumple la función de prueba proporcionada.
const getHeroeById = (id) => {
  return heroes.find((heroe) => heroe.id === id);
};

console.log(getHeroeById(1));
console.log(getHeroeById(2));
console.log(getHeroeById(3));
console.log(getHeroeById(4));
console.log(getHeroeById(5));

// Vemos como usar el metodo filter para traer todos los que cumplan la con condicion:
const getHeroeByOwner = (owner) => {
  return heroes.filter((heroe) => heroe.owner === owner);
};

console.log(getHeroeByOwner('DC'));
