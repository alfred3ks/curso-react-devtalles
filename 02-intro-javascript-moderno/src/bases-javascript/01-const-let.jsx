/*
Variables y Constantes:
Veremos como declarar variables y constantes:
Usar let y const para declarar variables, ya no usar declaraciones de varaibles con la palabra reservada var:
*/

const nombre = 'Luis';
const apellido = 'Marcano';
let edad = 21;

// Sobre escrbimos la variable:
edad = 22;

console.log(`Me llamo ${nombre} ${apellido} y tengo: ${edad} años`);

let condicion = true;

if (condicion) {
  /*
  Estas variables solo existe aqui dentro de las llaves, por el scope por bloque Eso lo aporta let y const
  */
  const nombre = 'Peter';
  let valorDado = 55;
  console.log(valorDado, nombre);
}
