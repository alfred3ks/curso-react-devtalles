/*
Async await:

`async` y `await` son características de JavaScript que simplifican la escritura de código asincrónico, especialmente cuando se trabaja con promesas. Estas características se introdujeron en ECMAScript 2017 (ES8) y se utilizan para hacer que el código asincrónico sea más legible y fácil de seguir, eliminando la necesidad de utilizar callbacks o promesas encadenadas.

- `async`: La palabra clave `async` se coloca antes de una función para indicar que esa función es asíncrona. Una función asíncrona siempre devuelve una promesa. Dentro de una función asíncrona, puedes utilizar la palabra clave `await` para pausar la ejecución de la función hasta que una promesa se resuelva o se rechace. Esto permite escribir código que parece sincrónico, pero en realidad es asincrónico bajo el capó.

- `await`: La palabra clave `await` se usa dentro de una función `async` y se coloca antes de una expresión que devuelve una promesa. Cuando se encuentra `await`, la ejecución de la función se pausa hasta que la promesa se resuelva o se rechace. Si la promesa se resuelve, el valor resuelto se asigna a la variable que sigue a `await`. Si la promesa se rechaza, se lanza una excepción que puede ser capturada utilizando un bloque `try...catch`.

*/
const apiKey = 'Sjdq94inQgHG0DKnnPPyAHwZDcOPBwah';
const endPoint = `https://api.giphy.com/v1/gifs/random?api_key=${apiKey}`;

const getImageAsync = async () => {
  try {
    const respuesta = await fetch(endPoint);
    const { data } = await respuesta.json();
    const { url } = data.images.original;
    console.log(url);

    const img = document.createElement('img');
    img.src = url;
    document.body.append(img);
  } catch (error) {
    console.error(error);
  }
};

getImageAsync();
