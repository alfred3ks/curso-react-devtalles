/*
Fetch API:
Tenemos una web que nos permite traer giphy para hacer pruebas, debemos loguearnos:
https://developers.giphy.com/
Ademas tenemos la documentacion de Mozilla sobre Fetch():
https://developer.mozilla.org/es/docs/Web/API/Fetch_API
*/

const apiKey = 'Sjdq94inQgHG0DKnnPPyAHwZDcOPBwah';
const endPoint = `https://api.giphy.com/v1/gifs/random?api_key=${apiKey}`;

const peticion = fetch(endPoint);

peticion
  .then((resp) => {
    return resp.json();
  })
  .then(({ data }) => {
    const { url } = data.images.original;

    const img = document.createElement('img');
    img.src = url;
    document.body.append(img);
  })
  .catch((err) => {
    console.log(err);
  });
