/*
Template String:
Podemos concatenar variables, hacer operaciones, etc.
*/

const nombre = 'Mario';
const apellido = 'Lopez';

// Como concatenar: Opcion 1:
let nombreCompleto = nombre + ' ' + apellido;
console.log(nombreCompleto);

// Usando Template String: Opcion 2:
// Dentro de la llaves podemos pasar una expresion que js evaluara.
nombreCompleto = `${nombre} ${apellido}`;
console.log(nombreCompleto);

console.log(`${2 + 2}`);

function getSaludo(nombre, apellido) {
  return `Hola ${nombre} ${apellido}`;
}

console.log(`Y la respuesta de la funcion es: ${getSaludo(nombre, apellido)}`);
