/*
Objects literals:
Objetos literales. Vamos lo que llamamos un objeto.

Lo definimos con las llaves {}

Trabajan con pares clave:valor.
*/

const persona = {
  nombre: 'Layka',
  apellido: 'Sánchez',
  edad: 2,
  direccion: {
    ciudad: 'New York',
    calle: 'Venus, 22',
    zipCode: 29009,
    lat: 14.7878,
    log: 34.656,
  },
};

console.log(persona);
console.log(persona.nombre);
console.log(persona.apellido);

// Creamos otro objeto, que dentro lleva otro objeto:
console.log({ persona });
console.table({ persona });

// Vamos a clonar el objeto anterior: Sin modificar el original:
// Con el operador spread ...nombre_objeto
const persona2 = { ...persona };
persona2.nombre = 'Peter';
console.log(persona2);
console.log(persona);
