/*
Arreglos - Arrays:

Un arreglo es una colección de información que se encuentra dentro de una misma variable.
*/

// Podemos crear un arreglo usando el constructor, esto no es muy usado:
const arr = new Array(10); // 10 posiciones vacias,
arr.push(23); // Luego añadimos un valor.
console.log(arr);

// Esto es lo mas común de ver: El método .push() altera el arreglo original:
const arr1 = [];
arr1.push(1);
arr1.push(2);
arr1.push(3);
arr1.push(4);

console.log(arr1);

// Lo ideal es usar el operador spread operator:
const arr2 = [...arr1];
arr2.push(5);
arr2.push(6);
arr2.push(7);

console.log(arr1);
console.log(arr2);

// Vamos a ver como iterar usando map(), muy usado en React.js, el método map() retorna un nuevo arreglo
const arr3 = [1, 2, 3, 4, 5, 6, 7, 8, 9, 0];
let arrDoble = arr3.map((num) => {
  return num * 2;
});
console.log(arr3);
console.log(arrDoble);
