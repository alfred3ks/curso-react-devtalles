/*
Operador ternario:

El operador ternario es un operador condicional en JavaScript y en muchos otros lenguajes de programación. También se conoce como el operador condicional ternario debido a su naturaleza de tres partes. El operador ternario se utiliza para tomar decisiones basadas en una expresión condicional y devuelve un valor según el resultado de esa expresión.

La sintaxis general del operador ternario es la siguiente:

```javascript
condición ? valorSiVerdadero : valorSiFalso;
```

- `condición`: Una expresión que se evalúa como verdadera o falsa.
- `valorSiVerdadero`: El valor que se devuelve si la condición es verdadera.
- `valorSiFalso`: El valor que se devuelve si la condición es falsa.

El operador ternario se utiliza comúnmente para asignar un valor a una variable en función de una condición. Aquí hay un ejemplo:

```javascript
const edad = 18;
const esMayorDeEdad = edad >= 18 ? 'Sí' : 'No';

console.log(esMayorDeEdad); // Esto imprimirá 'Sí' en la consola
```

En este ejemplo, la variable `esMayorDeEdad` se asigna a `'Sí'` si la edad es mayor o igual a 18, y a `'No'` en caso contrario.

El operador ternario también se puede utilizar en expresiones más complejas:

```javascript
const numero = 5;
const esPar = numero % 2 === 0 ? 'Sí' : 'No';

console.log(esPar); // Esto imprimirá 'No' en la consola
```

Aquí, la variable `esPar` se asigna a `'Sí'` si el número es par (divisible por 2) y a `'No'` en caso contrario.

El operador ternario es útil cuando se necesita tomar decisiones simples basadas en una condición y se puede utilizar para hacer que el código sea más conciso y legible. Sin embargo, cuando se tienen decisiones más complejas con múltiples ramificaciones, es común utilizar declaraciones `if...else` en lugar del operador ternario.

*/

// Veamos como lo hariamos de manera tradicional:
const activo = true;
let mensaje = '';

if (!activo) {
  mensaje = 'Activo';
} else {
  mensaje = 'Inactivo';
}
console.log(mensaje);

// Veamos usando el operador ternario:
const mensaje1 = activo ? 'Activo' : 'Inactivo';
console.log(mensaje1);

// Veamos otro ejemplo: Cuando solo es necesario responder true o false
const mensaje3 = activo && 'Activo';
const mensaje4 = !activo && 'Activo'; // Retorna false
console.log(mensaje3);
console.log(mensaje4);
