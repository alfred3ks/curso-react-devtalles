/*
Funciones:
Una función en JavaScript es similar a un procedimiento — un conjunto de instrucciones que realiza una tarea o calcula un valor, pero para que un procedimiento califique como función, debe tomar alguna entrada y devolver una salida donde hay alguna relación obvia entre la entrada y la salida.
*/

// Funcion como declaracion:
function saludar1(nombre) {
  return `Hola, ${nombre}`;
}
// Funcion como asignación:
const saludar2 = function (nombre) {
  return `Hola, ${nombre}`;
};

// Funcion como asignación pero flecha, si solo hace un retorn la ponemos asi:
const saludar3 = (nombre) => `Hola, ${nombre}`;

console.log(saludar1('Marcos'));
console.log(saludar2('Goku'));
console.log(saludar3('Vegeta'));

// OJO con esto es usado en react: Aqui vemos como quitar el return:
const getUser = () => {
  return {
    uid: 'ABC123',
    userName: 'goku23',
  };
};

const user = getUser();
console.log(user);

// Asi quitamos el return:
const getUser1 = () => ({
  uid: 'ABC123',
  userName: 'goku23',
});

const user2 = getUser1();
console.log(user2);

// Otro ejemplo:
// Transformar en funcion flecha,
// Tiene que retornar un objeto implicito,
// Probarlo:
const getUserActive = (name) => ({
  uid: 'ABC456',
  username: name,
});

const userActive = getUserActive('Mario');
console.log(userActive);
