/*
Desestructuración:
Podemos desestructurar objetos y arreglos:
https://developer.mozilla.org/es/docs/Web/JavaScript/Reference/Operators/Destructuring_assignment
*/

// Arreglos:
const personajes = ['Goku', 'Vegeta', 'Trunks'];

const [p1] = personajes;
console.log(p1);

// Si queremos algun en cuestion basta con dejar resto en vacio y separar por comas:
const [, p2] = personajes;
console.log(p2);

const [, , p3] = personajes;
console.log(p3);

// Veamoslo con una funcion: OJO esto es muy usado en hooks:
const retornaArr = () => {
  return ['ABC', 123];
};

const [letras, numeros] = retornaArr();
console.log(letras, numeros);

// Otro ejemplo: OJO esto es como si fuera useState(), para los estados en react.js
const userState = (valor) => {
  return [
    valor,
    () => {
      console.log(`Hola ${valor}`);
    },
  ];
};

const [nombre, setNombre] = userState('David');
console.log(nombre);
setNombre();
