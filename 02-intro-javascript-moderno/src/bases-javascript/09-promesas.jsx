/*
Promesas:
https://developer.mozilla.org/es/docs/Web/JavaScript/Reference/Global_Objects/Promise

En JavaScript, una promesa (Promise en inglés) es un objeto que representa la eventual finalización (o falla) de una operación asincrónica. Las promesas son una forma de gestionar operaciones asíncronas de manera más ordenada y manejable, evitando el uso excesivo de callbacks anidados, lo que a menudo se conoce como el "callback hell" o "infierno de callbacks".

Una promesa tiene tres estados posibles:

1. Pendiente (Pending): Cuando se crea una promesa, comienza en este estado. Esto significa que la operación aún no se ha completado ni ha fallado.

2. Resuelta (Fulfilled): La promesa pasa a este estado cuando la operación se ha completado con éxito. En este punto, se llama a la función `resolve` de la promesa, y se pueden manejar los resultados de la operación.

3. Rechazada (Rejected): La promesa pasa a este estado si la operación falla. En este caso, se llama a la función `reject` de la promesa, y se pueden manejar los errores.

Las promesas se utilizan comúnmente para realizar operaciones asincrónicas, como solicitudes HTTP, lectura/escritura de archivos o cualquier tarea que tome tiempo en completarse. Esto permite escribir código más limpio y legible en lugar de anidar múltiples callbacks.

Aquí hay un ejemplo de cómo se crea y utiliza una promesa en JavaScript:

const miPromesa = new Promise((resolve, reject) => {
  // Simular una operación asincrónica
  setTimeout(() => {
    const exito = true; // Cambia esto a 'false' para simular un fallo
    if (exito) {
      resolve("Operación completada con éxito");
    } else {
      reject("Operación fallida");
    }
  }, 2000); // Simulación de una operación que toma 2 segundos
});

// Usando la promesa
miPromesa
  .then((resultado) => {
    console.log(resultado); // Se ejecuta si la promesa se resuelve
  })
  .catch((error) => {
    console.error(error); // Se ejecuta si la promesa se rechaza
  });


En este ejemplo, la promesa `miPromesa` se resuelve después de 2 segundos y muestra un mensaje de éxito. Si cambias `exito` a `false`, la promesa se rechazaría y mostraría un mensaje de error. Esto ilustra cómo se manejan las resoluciones y rechazos de promesas utilizando `then` y `catch`.

*/

const promesa = new Promise((resolve, reject) => {
  setTimeout(() => {
    const good = true;
    if (good) resolve('Todo OK!!!');
    else reject('Ufffss ALGO FALLO!!!');
  }, 2000);
});

// Asi consumimos la promesa:
promesa
  .then((response) => {
    console.log(response);
  })
  .catch((err) => {
    console.log(err);
  })
  .finally(() => {
    console.log('Cerramos conexión.');
  });

// Ahora vamos a simular la conexion a una bd y traer informacion:
import heroes from './data/heroes';
// Tenemos la funcion que busca en bd: Si lo encuentra lo retorna:
const getHeroeById = (id) => {
  return heroes.find((heroe) => heroe.id === id);
};

const promesaHeroe = new Promise((resolve, reject) => {
  setTimeout(() => {
    const heroe = getHeroeById(1);
    if (heroe !== undefined) resolve(heroe);
    else reject('El heroe no existe!!!');
  }, 3000);
});

// Asi consumimos la promesa:
promesaHeroe
  .then((response) => {
    console.log('Heroe:', response);
  })
  .catch((err) => {
    console.warn(err);
  })
  .finally(() => {
    console.log('Cerramos conexión.');
  });

// Creamos mejor una funcion que lo ejecuta todo y le pasasmos por parametro el id:
const getHeroeAsync = (id) => {
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      const heroe = getHeroeById(id);
      heroe !== undefined ? resolve(heroe) : reject('El heroe no existe!!!');
    }, 4000);
  });
};

getHeroeAsync(5)
  .then((response) => {
    console.log('Heroe:', response);
  })
  .catch((err) => {
    console.warn(err);
  })
  .finally(() => {
    console.log('Cerramos conexión.');
  });
