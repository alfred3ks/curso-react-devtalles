/*
Desestructuración:
Podemos desestructurar objetos y arreglos:
https://developer.mozilla.org/es/docs/Web/JavaScript/Reference/Operators/Destructuring_assignment
*/

// Objetos:
const persona = {
  nombre: 'Tony',
  edad: 45,
  username: 'Ironman',
};

// Asi hacemos desestructuración del objeto: Podemos extraer del objeto aquello que necesitemos:
const { nombre, edad, username } = persona;
console.log(nombre, edad, username);

// Vemos como desestructuramos y lo pasamos como parametro a la funcion:
// Tambien vemos que podemos asignar valores por defecto:
const retornaPersona = ({ nombre, edad = 23, rango = 'Capitan', username }) => {
  console.log(nombre, edad, username, rango);
};

retornaPersona(persona);

// Esto tambien he visto bastante en react.js:
const usarContext = ({ nombre, edad, rango = 'Capitan', username }) => {
  return {
    nombreClave: nombre,
    annios: edad,
    latlng: {
      lat: 14.1232,
      lng: -12.3232,
    },
  };
};

const {
  nombreClave,
  annios,
  // asi podemos extraer objetos anidados:
  latlng: { lat, lng },
} = usarContext(persona);

console.log(nombreClave, annios, lat, lng);
