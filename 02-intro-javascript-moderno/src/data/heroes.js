const heroes = [
  {
    id: 1,
    name: 'Batman',
    owner: 'DC',
  },
  {
    id: 2,
    name: 'Spiderman',
    owner: 'Marvel',
  },
  {
    id: 3,
    name: 'Superman',
    owner: 'DC',
  },
  {
    id: 4,
    name: 'Flash',
    owner: 'DC',
  },
  {
    id: 5,
    name: 'Wolverine',
    owner: 'Marvel',
  },
];

export let suma = (a, b) => {
  return a + b;
};

/*
Tenemos varias formas de exportar un archivo:
- Export por default:
export default heroes;

- Si tenemos mas elementos podemos exportarlos asi:
export {
  heroes,
  suma
}

- Si tenemos mas elementos podemos exportarlos asi y asignar uno por defecto:
export {
  heroes as default,
  suma
}

- Podemos exportar solo un elemento:
export const heroes = [{}]
export let suma = (a, b) => {
  return a + b;
};

*/
export default heroes;
