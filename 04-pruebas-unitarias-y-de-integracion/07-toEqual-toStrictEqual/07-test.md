# toEqual() y toStrictEqual():

Vamos aseguir haciendo pruebas, siguiente archivo a testear, el 05-funciones.

Aqui vamos a ver como testear funciones con objetos, muy importante esto porque antes era con string, ahora tenemos un objeto a evaluar:
Para eso tenemos dos métodos nuevos el toEqual y el toStrictEqual().

El código nos queda asi de esta manera:

## 05-funciones-test.js:

```javascript:
import { getUser, getUsuarioActivo } from '../../src/base-pruebas/05-funciones';

describe('Pruebas en 05-funciones', () => {

  test('getUser debe retornar un objeto con uid y username', () => {
    const testUser = {
      uid: 'ABC123',
      username: 'El_Papi1502',
    };
    const user = getUser();

    expect(user).toEqual(testUser);
  });


  test('getUsuarioActivo debe retornar un objeto con uid y username', () => {
    const nombre = 'Fernando';
    const user = getUsuarioActivo(nombre);
    const testUser = {
      uid: 'ABC567',
      username: nombre,
    };

    expect(user).toStrictEqual(testUser);
  });
});
```
