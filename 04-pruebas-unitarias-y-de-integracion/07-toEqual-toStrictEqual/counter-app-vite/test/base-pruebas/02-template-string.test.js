import { getSaludo } from '../../src/base-pruebas/02-template-string';

// eslint-disable-next-line no-undef
describe('Pruebas en 02-template-string', () => {
  // eslint-disable-next-line no-undef
  test('getSaludo debe retornar "Hola Luis"', () => {
    const name = 'Luis';
    const message = getSaludo(name);

    // eslint-disable-next-line no-undef
    expect(message).toBe(`Hola ${name}`);
  });
});
