import { getUser, getUsuarioActivo } from '../../src/base-pruebas/05-funciones';

// eslint-disable-next-line no-undef
describe('Pruebas en 05-funciones', () => {
  // eslint-disable-next-line no-undef
  test('getUser debe retornar un objeto con uid y username', () => {
    const testUser = {
      uid: 'ABC123',
      username: 'El_Papi1502',
    };
    const user = getUser();

    // eslint-disable-next-line no-undef
    expect(user).toEqual(testUser);
  });

  // eslint-disable-next-line no-undef
  test('getUsuarioActivo debe retornar un objeto con uid y username', () => {
    const nombre = 'Fernando';
    const user = getUsuarioActivo(nombre);
    const testUser = {
      uid: 'ABC567',
      username: nombre,
    };

    // eslint-disable-next-line no-undef
    expect(user).toStrictEqual(testUser);
  });
});
