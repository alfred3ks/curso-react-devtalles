# Pasos para instalar y hacer test con Jest.js:

1. Instalar Jest.js como dependencia de desarrollo en nuestro proyecto de react:

```bash
npm install -D jest
```

2. Crear el script para arrancar jest y testear, lo haremos en el package.jons en la zona de script, recordar agregar el modificardor --watchAll para que este constante observando cambios al arrancar los test:

```json
"scripts": {
    "test": "jest --watchAll"
  },
```

3. Instalar como dependencia de desarrollo la ayuda para vscode para usar las funciones de Jest.js y todos sus métodos:

```bash
npm install -D @types/jest
```

```json
"devDependencies": {
    "@types/jest": "^29.5.8",
  }
```

4. Configuración de Jest.js para Babel, asi podemos testear código moderno.

Para esto necesitamos ejecutar el siguiente comando que nos dice la documentación de Jest.js, tambien se instala como dependecia de desarrollo.

```bash
npm install --save-dev babel-jest @babel/core @babel/preset-env
```

Ahora segun dice la documentación debemos crear un archivo de confiración de babel: lo llamaremos bebel.config.cjs en la raiz del proyecto.

```javascript
module.exports = {
  presets: [['@babel/preset-env', { targets: { node: 'current' } }]],
};
```

# Filtrar archivo a testear:

Cuando tenemos el modo -watchAll en la consola este esta observando todos los archivos de test, eso para dos archivos esta bien pero si tenemos muchos esto no es viable, si queremos hacer test a un archivo en particular procedemos asi:

- pulsamos w, y vemos que se nos despliegan varias opciones,
- para filtrar por un archivo pulsamos p
- escribimos el nombre del archivo que vamos a testear. No hace falta poner todo el nombre con que pongamos parte de el jest.js lo buscar y realiza el test.

Ahora ya estamos testeando un archivo en particular.
