# Continuamos con las pruebas.

Veremos la funcion expect() y el método toBe().

Lo haremos con el archivo dentro de la carpeta test. demo.test.js.

## Podemos recordar que hay tres pasos que debemos hacer en las pruebas:

1. inicialización:
2. estimulo:
3. observar el comportamiento esperado:

Para este caso lo que hacemos es comparar dos cadenas. Algo sencillo. Podemos ver que para hacer la comparación se usa la funcion expect(message1), recibe por parametro lo que se espera, y luego tenemos el metodo toBe(message2), que es lo que se recibe. Se recibe algo y se espera que sea esto.

Ahora para ayudarnos con las funciones y metodos que usa jest vamos a installar una dependencia. En desarrollo.

Vamos a installar una dependencia de desarrollo que nos va ayudar a tener el intellince para poder sacar las funciones y metodos de jest, asi no tendremos problemas para cuando escribamos las funciones y los metodos.

```bash
npm install -D @types/jest
```

Ahora ya tenemos la ayuda de vscode.

Cuando montamos nuestra prueba lo ideal seria agruparlas, para eso tenemos una funcion, describe(), recibe como primer parametro una descripcion de la prueba, y como callback una funcion donde ejecutaremos la prueba. Lo podemos ver a continuacion.

```javascript
describe('Pruebas en <demoComponent></demoComponent> ', () => {
  test('Aqui va la descripcion de la prueba', () => {
    // 1. inicialización:
    const message1 = 'Hola mundo';
    // 2. estimulo:
    const message2 = message1.trim();
    // 3. observar el comportamiento esperado:
    expect(message1).toBe(message2);
  });
});
```

```bash
npm run test
```

Al correr la prueba para este caso vemos que pasa, pero si agregamos unos espacios al primer mensaje vemos como nuestro test no pasa, porque espera una cosa y recibe otra.
