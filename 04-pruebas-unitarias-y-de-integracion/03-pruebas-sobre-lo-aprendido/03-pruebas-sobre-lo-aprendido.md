# Pruebas sobre lo aprendido:

Tenemos en este apartado una carpeta llamada base-pruebas donde tenemos algunos de los ejercicios vistos en intro de javascript. Estos ejercicios los suaremos para hacer las pruebas. Esta carpeta la vamos a colocar dentro de la carpeta src de la app de contador que venimos creando. counter-app-vite.

Lo que haremos es comenzar con lo más básico, donde no esta React.js.

Arrancamos nuestra app:

```bash
$ npm run dev
```
