# Introducción a las pruebas unitarias y de integración:

¿ Que son las pruebas?

Las pruebas NO son una perdida de tiempo.
Existen dos tipos de pruebas, las **_unitarias_** y de **_integración_**.

- Pruebas unitarias:
  Estan enfocadas en pequeñas funcionalidades. son pequeñas piezas de la app que deben ser probadas.

- Pruebas de integración:
  Estan enfocadas en como reaccionan varias piezas en conjunto.

¿Para que sirven?

Hacer testing del funcionamiento de nuestra aplicación, ver su comportamiento.

# Características de una prueba:

1. Fáciles de escribir,
2. Fáciles de leer,
3. Confiables,
4. Rápidas,
5. Principalmente unitarias.

Lo que haremos es comenzar a realizar pruebas unitarias a nuestra app, si tenemos las pruebas unitarias luego es mas fácil hacer la prueba de integración, si todas las unitarias funcionan la de integracion sera mas fácil y comprobar que todo funciona correctamente.
Los pasos ha realizar son conocidos como **_A A A_**:

- Arrange(Arreglar),
- Act(Actuar),
- Assert(Afirmar).

## Arrange(Arreglar):

Preparamos el estado inicial, usualmente se conoce como el objeto a probar.
Normalmente aqui:

- Inicializamos variables,
- Importaciones necesarias,
- En general probamos el ambiente a probar.

## Act(Actuar):

Aplicamos acciones o estímulos al sujeto de pruebas.

- Llamar a métodos,
- Simular clicks,
- Realizar acciones sobre el paso anterior.

## Assert(Afirmar):

Observar el comportamiento resultante del sujeto a pruebas.

- ¿Son los resultados los esperados?

# Mitos que rodean las a las pruebas:

- Hacen que mi aplicación no tenga errores:

Falso, tantos las pruebas como el propio código de la app son escritas por muchas veces por el mismo desarrollador, somos humanos y nada es perfecto.

- Las pruebas no pueden fallar:

Falso: Se pueden hacer pruebas que den falsos positivos o falsos negativos.

- Hacen mas lenta mi aplicación:

Falso: Las pruebas se hacen en la máquina de desarrollo nunca en producción.

- Son una perdida de tiempo:

Falso: Mas tiempo se perdera si nuestra app sube a producción y tenemos un bug, imagina si ese bug es en una parte de la app que recibe pagos. La consecuencia podria ser grave.

- Hay que probar todo:

Puede ser cierto o puede que no, si probamos todo nos podria ocupar mas tiempo que el que ocupamos para crear la aplicación, si estamos cerca de la entrega del proyecto lo ideal seria probar la ruta critica de la aplicación, las cracteristicas principales de la misma. Debemos ser eficientes en la parte de las pruebas.
