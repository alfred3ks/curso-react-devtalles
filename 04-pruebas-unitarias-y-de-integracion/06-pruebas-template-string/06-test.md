# Pruebas al template string.

Los tenemos en src/base-pruebas/02-template-string.js

NOTA: Para poder hacer pruebas estas solo se pueden hacer en cosas que sean exportadas, como es el caso aqui tenemos una funcion que tiene la instrucción export.

Creamos dentro de test la carpeta base-pruebas

Es bastante normal ver que el archivo de test se llama igual que el archivo que esta testeando, algo asi: 02-template-string.test.js

Ahora si vemos al arrancar el script npm run test, todos los archivos que esten en la carpeta test se ejecutaran.
Antes debemos tener instalado babel, lo vemos como hacerlo en el archivo 00-pasos-hacer-test.md.

Si ejecutamos nuestro test vemos como funciona correctamente. Nuestro archivo de test queda asi:

## 02-template-string.test.js:

```javascript
import { getSaludo } from '../../src/base-pruebas/02-template-string';

describe('Pruebas en 02-template-string', () => {
  test('getSaludo debe retornar "Hola Luis"', () => {
    const name = 'Luis';
    const message = getSaludo(name);

    expect(message).toBe(`Hola ${name}`);
  });
});
```

Notaremos que debemos importar el archivo a testear en la cabecera, tambien vemos como ejecutamos la funcion describe() y dentro aplicamos el test().
