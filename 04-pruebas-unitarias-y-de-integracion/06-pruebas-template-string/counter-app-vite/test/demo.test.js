/*
Debemos agrupar las pruebas para esto tenemos la funcion describe, donde podemos poner un titulo a la prueba, en el cuerpo pasamos la prueba a realizar. Esto esta muy bien porque en la consola la salida nos muestra la descripcion de dicha prueba.
*/
// eslint-disable-next-line no-undef
describe('Pruebas en <demoComponent></demoComponent> ', () => {
  // eslint-disable-next-line no-undef
  test('Aqui va la descripcion de la prueba', () => {
    // 1. inicialización:

    const message1 = 'Hola mundo';

    // 2. estimulo:
    const message2 = message1.trim();

    // 3. observar el comportamiento esperado:
    /*
  Con jest tenemos la funcion expect() y el metodo .tobe(), asi podemos comparar que la cadena del mensaje1 es igual a la cadena del mensaje2.
  Aqui la prueba espera lo que se pasa por el metodo .toBe(), es como realizar una comparacion.
  Todos estos metodos los podemos ver en la documentacion, tambien si instalamos la ayuda de jest en vscode.
  */
    // eslint-disable-next-line no-undef
    expect(message1).toBe(message2);
  });
});
