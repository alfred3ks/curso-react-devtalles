# Primeras pruebas y configuraciones iniciales:

Si estuvieramos dentro de un proyecto CRA, muchas configuraciones ya viene listas para correr, pero al ser un proyecto de vite.js debemos preparar ciertas configuraciones iniciales.

Vamos a ver el framework de jest.js, es un framework para hacer testing a aplicaciones de JavaScript. Vemos su web:

jest.io: https://jestjs.io/

Tambien tenemos React testing library, entre jest y este anterior se complementan muy bien para hacer test.

Vamos a instalar en el proyecto counter-app-vite jest. La instalamos como dependencia de desarrollo en el proyecto:

```bash
$ npm install jest -D
```

Vemos en el package.json que se ha instalado:

```json
{
  "devDependencies": {
    "@types/react": "^18.2.15",
    "@types/react-dom": "^18.2.7",
    "@vitejs/plugin-react": "^4.0.3",
    "eslint": "^8.45.0",
    "eslint-plugin-react": "^7.32.2",
    "eslint-plugin-react-hooks": "^4.6.0",
    "eslint-plugin-react-refresh": "^0.4.3",
    "jest": "^29.7.0",
    "vite": "^4.4.5"
  }
}
```

Ahora en el package.son debemos crear el script para arrancar los test, asi cuando ejecutemos estara siempre observando los cambios:

```json
"scripts": {
  "dev": "vite",
  "build": "vite build",
  "lint": "eslint . --ext js,jsx --report-unused-disable-directives --max-warnings 0",
  "preview": "vite preview",
  "test": "jest"
}
```

Ahora el comando para correr los test seria:

NOTA: No es neceario tener la app corriendo cuando hacemos los test.

```bash
npm run test
```

Si hacemos esto no dara un error, ya que no tenemos ninguna prueba, procedemos a crear nuestra primera prueba. Dentro de la **_raiz_** del proyecto vamos a crear una carpeta llamada test. Esta carpeta test es como un espejo de la carpeta src. Aqui es donde vamos a realizar los test.

## Vamos a crear dentro de test un archivo llamado demo.test.js.

```javascript
// La funcion test recibe un testo que es la descripcion de la prueba y una funcion callback
test('Aqui va la descripcion de la prueba', () => {
  //En el cuerpo de la funcion hacemos las comprobaciones
  if (0 === 0) {
    throw new Error('No se puede dividir por cero');
  }
});
```

Ahora lo que vamos ahacer es que cuando ejecutemos el comando npn run jest este este observando los cambio y no tener que estar ejecutando lcada rato:

```json
"scripts": {
  "dev": "vite",
  "build": "vite build",
  "lint": "eslint . --ext js,jsx --report-unused-disable-directives --max-warnings 0",
  "preview": "vite preview",
  "test": "jest --watchAll"
}
```

```bash
npm run test
```

Al hacer esto vemos que la consola nos ofrece varias acciones a realizar:

```bash
Watch Usage
 › Press f to run only failed tests.
 › Press o to only run tests related to changed files.
 › Press p to filter by a filename regex pattern.
 › Press t to filter by a test name regex pattern.
 › Press q to quit watch mode.
 › Press i to run failing tests interactively.
 › Press Enter to trigger a test run.
```

Seguimos...
