## ¿Que es react?

Es una libreria que nos permite crear aplicaciones.
Es declarativa, hace facil seguir patrones de diseño, es muy eficiciente.
React trabaja de manera predecible, ya que la informacion viaja en una sola via, ademas trabaja con componentes, que son piezas de código encapsuladas.

Con React.js se puede trabajar del lado del servidor, ademas podemos crear aplicaciones moviles con React native.

Aqui tenemos la documentación de React.js, un punto de consulta muy necesario cuando trabajemos con esta libreria:

https://es.react.dev/

Asi luce un código de React.js:

```html
<div id="root"></div>
```

```javascript
const divRoot = document.querySelector('#root);
const h1Tag = <h1>Hola devs</h1>;
ReactDOM.render(h1Tag, divRoot);
```

Toda esta mezcla de algo que parece HTML y JS, es lo que se conoce como JSX = JS + XML.

Mira como seria crear el tag h1 con react:

```javascript
const h1Tag = document.createElement('h1, null, 'Hola devs);
```

Hemos visto como crear la aplicacion mas sencilla usando React.js por medio de una CDN.
Para poder usar React.js mediante CDN debemos al div donde se va a renderizar la aplicacion le tenemos que decir que babel se encargue de procesaar el código y eso lo hacemos con el atributo type.

Podemos ver como podemos interpolar variables en código JSX.
Bastante sencillo pero bastante impresionante.

Vamos a explicar que es babel:
Babel es un "compilador" (o transpilador) para JavaScript.
Básicamente permite transformar código escrito con las últimas y novedosas características de JavaScript y transformarlo en un código que sea entendido por navegadores más antiguos.

React.js por detras usa babel. De esta manera podemos usar codigo moderno de JavaScript pero nosotros no tenemos que peocuparnos por la compatibilidad con navegadores antiguos ya que babel hara su magia.

En su web podemos ver como funciona:

https://babeljs.io/
